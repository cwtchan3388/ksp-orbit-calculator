from planet_info.body import Body
from kerbin import Kerbin


class Minmus(Body):
    """Minmus"""
    _instance = None
    standard_gravitational_parameter = 1.7658000e9
    sphere_of_influence = 2247428.4
    synchronous_orbit = 357940.0
    parent = Kerbin
    has_atmosphere = False
    max_surface_height = 5000