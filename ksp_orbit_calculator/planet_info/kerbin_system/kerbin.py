"""Kerbin definition"""
from kerbol_system.kerbol import Kerbol
from planet_info.body import Body


class Kerbin(Body):
    """Kerbin"""
    _instance = None
    standard_gravitational_parameter = 3.5316000e12
    sphere_of_influence = 84159286.0
    synchronous_orbit = 2863330.0
    parent = Kerbol
    has_atmosphere = True
    atmospheric_height = 70000
    max_surface_height = None
