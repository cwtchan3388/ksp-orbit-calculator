from planet_info.body import Body
from kerbin import Kerbin


class Mun(Body):
    """Mun"""
    _instance = None
    standard_gravitational_parameter = 6.5138398e10
    sphere_of_influence = 2429559.1
    synchronous_orbit = 2970560.0
    parent = Kerbin
