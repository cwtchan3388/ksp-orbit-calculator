"""Module that contains root body class"""
from abc import ABC
from datetime import timedelta
import math
from typing import Union



class Body(ABC):
    """Abstract class for celestial bodies"""
    _instance: None
    standard_gravitational_parameter: float  # in m^3s^-2
    sphere_of_influence: float  # m
    synchronous_orbit: float  # m
    parent: Union[Body, None]
    has_atmosphere: bool
    atmospheric_height: Union[float, None]  # m
    max_surface_height: Union[float, None]  # m


    def __new__(cls):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        
        return cls._instance

    @property
    def synchronous_orbital_period(self):
        """Determine the orbital period for a synchronous orbit"""
        return timedelta(seconds=math.sqrt(4 * math.pi**2 * self.synchronous_orbit**3))

    @property
    def min_periapsis(self):
        """Returns the height of the min periapsis"""
        if self.has_atmosphere:
            return self.atmospheric_height
        return self.max_surface_height

    @property
    def min_periapsis_reason(self):
        """Reason and height of min periapsis"""
        if self.has_atmosphere:
            return f"atmospheric height of {self.atmospheric_height}m"
        return f"max surface feature height of {self.max_surface_height}m"

    def __str__(self):
        return self.__class__.__name__
