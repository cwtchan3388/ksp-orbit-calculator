"""Kerbol info"""
from planet_info.body import Body


class Kerbol(Body):
    """Kerbol"""
    _instance = None
    standard_gravitational_parameter = 1.1723328e18
    sphere_of_influence = 0.0
    synchronous_orbit = 1508045290.0
    parent = None
    has_atmosphere = True
    atmospheric_height = 600000
