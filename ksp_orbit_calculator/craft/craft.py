"""Hold the craft class"""
from datetime import timedelta
from functools import lru_cache
import math
from typing import Union

from planet_info.body import Body

from exceptions import ApoapsisException, NoCraftParentException, PeriapsisException


class Craft:
    """A craft"""
    def __init__(
        self,
        name: str,
        apoapsis: float = 0.0,
        periapsis: float = 0.0,
        parent: Union[Body, None] = None
    ):
        self.name = name
        self._parent = parent
        self._apoapsis = apoapsis
        self._periapsis = periapsis

    @property
    def parent(self) -> Body:
        """Parent property"""
        if self._parent is None:
            raise NoCraftParentException(f"{self.name} has no parent set")

        return self._parent

    @parent.setter
    def _(self, value: Body):
        self._parent = value

    @property
    def apoapsis(self) -> float:
        """The current apoapsis"""
        if math.isclose(self._apoapsis, 0.0):
            raise ApoapsisException(f"{self.name} has no apoapsis set")

        return self._apoapsis

    @apoapsis.setter
    def _(self, value: float) -> None:
        if value > self.parent.sphere_of_influence:
            raise ApoapsisException(f"value: {value} is greater {self.parent}'s "
                                    f"SOI of {self.parent.sphere_of_influence}")
        if value < self.parent.min_periapsis:
            raise ApoapsisException(f"value: {value} is lower than {self.parent}'s "
                                    f"{self.parent.min_periapsis_reason}")
        if value < self.periapsis:
            self._apoapsis = self.periapsis
            self.periapsis = value
            return None

        self._apoapsis = value

    @property
    def periapsis(self) -> float:
        """The current periapsis"""
        if math.isclose(self._periapsis, 0.0):
            raise PeriapsisException(f"{self.name} has no periapsis set")

        return self._periapsis

    @periapsis.setter
    def _(self, value: float) -> None:
        if value < self.parent.min_periapsis:
            raise PeriapsisException(f"value: {value} is lower than {self.parent}'s "
                                     f"{self.parent.min_periapsis_reason}")
        if value > self.parent.sphere_of_influence:
            raise PeriapsisException(f"value: {value} is greater {self.parent}'s "
                                    f"SOI of {self.parent.sphere_of_influence}")
        if value > self.apoapsis:
            self._periapsis = self.apoapsis
            self.apoapsis = value
            return None

        self._periapsis = value

    @property
    @lru_cache
    def semi_major_axis(self) -> float:
        """The semi-major axis of the orbit"""
        return (self.apoapsis + self.periapsis) / 2

    @property
    @lru_cache
    def eccentricity(self) -> float:
        """Orbit eccentricity"""
        return (self.apoapsis - self.periapsis) / (self.apoapsis + self.periapsis)

    @property
    @lru_cache
    def orbital_period(self) -> timedelta:
        """Returns the orbital period"""
        return timedelta(seconds=math.sqrt(4 * math.pi**2 * self.semi_major_axis**3 /
                                           self.parent.standard_gravitational_parameter))
