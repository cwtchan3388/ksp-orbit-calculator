"""Craft specific exceptions"""


class CraftException(Exception):
    """Base craft exception"""


class NoCraftParentException(CraftException):
    """Exception if no parent"""


class ApoapsisException(CraftException):
    """Exception for apoapsis"""


class PeriapsisException(CraftException):
    """Exception for periapsis"""
