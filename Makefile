.PHONY: all
which_python := $(shell which python)

all: check_python make_env

check_python:
ifeq ($(strip ${which_python}),)
	@echo "no python"
else
	echo "Ha"
endif

make_env:
	# Make the virtual env
	OUTPUT=$(python --version)
	echo ${OUTPUT}
