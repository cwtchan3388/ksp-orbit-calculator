#!/usr/bin/env python

from distutils.core import setup


setup(name="ksp_orbit_calculator",
      version="0.1",
      description="KSP Orbit Calculator",
      author="Chris Chan",
      author_email="cwtchan3388@gmail.com",
      url="https://gitlab.com/cwtchan3388/ksp-orbit-calculator",
      packages=["ksp_orbit_calculator"])
